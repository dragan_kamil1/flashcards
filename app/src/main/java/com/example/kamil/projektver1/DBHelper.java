package com.example.kamil.projektver1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Flashcard.db";
    // Pierwsza tabela - Carts
    public static final String CARTS_TABLE_NAME = "Carts";
    public static final String CARTS_COLUMN_ID = "id";
    public static final String CARTS_COLUMN_POLISH_CONTENT = "Polish_Content";
    public static final String CARTS_COLUMN_ENGLISH_CONTENT = "English_Content";
    public static final String CARTS_COLUMN_CATEGORY = "Category";
    // Druga tabela - Category
    public static final String CATEGORY_TABLE_NAME = "Category";
    public static final String CATEGORY_COLUMN_CATEGORY = "category";
    public static final String CATEGORY_COLUMN_LASTSCORE = "LastScore";
    public static final String CATEGORY_COLUMN_AVARAGESCORE = "AvarageScore";


    public DBHelper(Context context) {

        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //tworzenie pierwszej tabeli - Carts
        db.execSQL(
                "create table Carts " +
                        "(id integer primary key, Polish_Content text,English_Content text,Category text)"
        );

        //tworzenie drugiej tabeli - Category
        db.execSQL(
                "create table Category " +
                        "(Category text, LastScore text, AvarageScore text, NumberTrials text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Carts");
        db.execSQL("DROP TABLE IF EXISTS Category");
        onCreate(db);
    }

    public boolean insertCarts (String polish_content, String english_content, String category)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CARTS_COLUMN_POLISH_CONTENT, polish_content);
        contentValues.put(CARTS_COLUMN_ENGLISH_CONTENT, english_content);
        contentValues.put(CATEGORY_COLUMN_CATEGORY, category);
        db.insert(CARTS_TABLE_NAME, null, contentValues);
        return true;
    }
    public boolean insertCategory (String category, String lastScore, String avarageScore, String numberTrials)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CATEGORY_COLUMN_CATEGORY, category);
        contentValues.put(CATEGORY_COLUMN_LASTSCORE, lastScore);
        contentValues.put(CATEGORY_COLUMN_AVARAGESCORE, avarageScore);
        contentValues.put(CATEGORY_COLUMN_AVARAGESCORE, numberTrials);
        db.insert(CATEGORY_TABLE_NAME, null, contentValues);
        return true;
    }
    /*public boolean deleteCategory (String category)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(CATEGORY_TABLE_NAME, CATEGORY_COLUMN_CATEGORY + "=" + category, null) > 0;
    }*/
    public void deleteCategory (String category)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("Category", "category = ? ", new String[] {category});
    }
    public Cursor getDataFromCategory(){
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery( "SELECT * FROM " + CATEGORY_TABLE_NAME , null );
    }
    public boolean isCategoryOnDB(String category){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM Category WHERE  category = '" + category + "'", null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

}
