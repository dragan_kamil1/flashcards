package com.example.kamil.projektver1;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class DeleteCategoryActivity extends AppCompatActivity {

    DBHelper mydb;
    private ArrayAdapter myAdapter;
    final ArrayList<String> list = new ArrayList<>();
    String categoryName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_category);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ListView listview = (ListView) findViewById(R.id.listView2);
        mydb = new DBHelper(this);

        Cursor cursor = mydb.getDataFromCategory();
        if (cursor != null) {
            // move cursor to first row
            if (cursor.moveToFirst()) {
                do{
                    // Get version from Cursor
                    String bookName = cursor.getString(cursor.getColumnIndex("Category"));
                    // add the bookName into the bookTitles ArrayList
                    list.add(bookName);
                    // move to next row
                }while (cursor.moveToNext());
            }
        }
        myAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
        listview.setAdapter(myAdapter);
        listview.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        listview.setSelector(android.R.color.holo_blue_light);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView, int myItemInt, long mylng) {
                categoryName = ((TextView) myView).getText().toString();
            }
        });

        final Button delete_category = (Button) findViewById(R.id.usun_kategorie);
        delete_category.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                if (categoryName == null)
                    Toast.makeText(getApplicationContext(), "Nie wybrałeś kategorii. Zaznacz kategorię.", Toast.LENGTH_LONG).show();
                else {
                    mydb.deleteCategory(categoryName);

                    Toast msg = Toast.makeText(getBaseContext(), "Kategoria usunięta", Toast.LENGTH_LONG);
                    msg.show();

                    Intent intent = new Intent(DeleteCategoryActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

}
