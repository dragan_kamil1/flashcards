package com.example.kamil.projektver1;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddCategoryActivity extends AppCompatActivity {

    DBHelper mydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);

        mydb = new DBHelper(this);
        final Button save_category = (Button) findViewById(R.id.zapisz_kategorie);
        save_category.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                EditText getCategory = (EditText) findViewById(R.id.getCategoryName);
                String category = getCategory.getText().toString();

                if (!mydb.isCategoryOnDB(category)) {
                    mydb.insertCategory(category, "0", "0", "0");

                    Toast msg = Toast.makeText(getBaseContext(), "Kategoria dodana", Toast.LENGTH_LONG);
                    msg.show();

                    Intent intent = new Intent(AddCategoryActivity.this, AddCategoryActivity.class);
                    startActivity(intent);
                } else {
                    Toast msg = Toast.makeText(getBaseContext(), "Podana nazwa kategorii już istnieje. Podaj inną", Toast.LENGTH_LONG);
                    msg.show();
                }
            }
        });



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

}
